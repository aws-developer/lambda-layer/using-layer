const {responseHttp} = require("core-logic");

module.exports.hello = async (event) => {
  return responseHttp(200, "Success", true);
};
